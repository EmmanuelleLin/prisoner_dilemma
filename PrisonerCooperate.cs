namespace PrisonerDilemma;

/// <summary>
/// Always cooperates
/// </summary>
class PrisonerCooperate : IPrisoner
{
  public string Name { get; } = "Cooperator";

  public void Reset(DilemmaParameters dilemmaParameters) { }

  public PrisonerChoice GetChoice()
  {
    return PrisonerChoice.Cooperate;
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) { }
}