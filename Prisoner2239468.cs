using static PrisonerDilemma.PrisonerRandom;
namespace PrisonerDilemma;
class Prisoner2239468 : IPrisoner{
    public string Name => "Emmanuelle";

    private static Random Random { get; } = new Random();

    public PrisonerChoice GetChoice(){
        return Random.Next(2) < 1 ?
        PrisonerChoice.Cooperate : PrisonerChoice.Defect;
    }

    public void ReceiveOtherChoice(PrisonerChoice otherChoice){
    }
 
    public void Reset(DilemmaParameters dilemmaParameters){
    }
}